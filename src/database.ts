import mongoose from 'mongoose';

mongoose.connect(process.env.MONGODB_URI || 'mongodb+srv://prem:prem@cluster0-vkuo6.mongodb.net/assignment?retryWrites=true&w=majority', {
    useCreateIndex: true,
    useNewUrlParser: true,
    useFindAndModify: true,
    useUnifiedTopology: true
})
    .then(db => console.log('Database is connected'))
    .catch(err => {
        console.log(err)
    });

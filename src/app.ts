import express, { Application } from 'express';
import morgan from 'morgan';
import './database';
import dotenv from 'dotenv';

dotenv.config();

const app: Application = express();

import authRoutes from './routes/auth';
import productRoutes from './routes/product';


//settings
app.set('port', 4000);


app.use(morgan('dev'));
app.use(express.json());

//routes
app.use('/api/auth', authRoutes);
app.use(productRoutes);

app.listen(app.get('port'));
console.log("server on port", app.get('port'));

export default app;
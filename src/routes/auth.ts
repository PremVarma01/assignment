import { Router } from 'express';
const router: Router = Router();
import { TokenValidation } from '../libs/validateToken';
import { signIn, signUp, profile } from '../controllers/auth.controller';

router.post('/signup', signUp);
router.post('/signin', signIn);
router.get('/profile', TokenValidation, profile);

export default router;
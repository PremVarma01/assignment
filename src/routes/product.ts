import { Router } from 'express';
import { TokenValidation } from '../libs/validateToken';
import { addProduct, getProducts, addToCart, addToCartUserModal, getCart } from '../controllers/product.controller';

const router: Router = Router();


router.post('/product', addProduct);
router.get('/products', getProducts);
router.post('/user/cart', TokenValidation, addToCartUserModal);
router.post('/cart', TokenValidation, addToCart);
router.get('/cart', getCart);

export default router;
import { Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';

interface RequestWithUser extends Request {
    userId?: string;
}

interface IPayload {
    _id: string,
    iat: number,
    exp: number
}

export const TokenValidation = (req: RequestWithUser, res: Response, next: NextFunction) => {
    const token = req.header('authToken');
    if (!token) return res.status(401).json({ 'message': 'Sorry!, Access denied!' });
    try {
        const payload = jwt.verify(token, process.env.SECRET_TOKEN || 'heyletsstart') as IPayload;
        req.userId = payload._id;
        next();
    } catch (err) {
        res.json({ 'message': 'This token is not valid or expired!' });
    }
}
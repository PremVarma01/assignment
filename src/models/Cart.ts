import { Schema, model, Document } from 'mongoose';

export interface ICart extends Document {
    userId: Schema.Types.ObjectId;
    productId: Schema.Types.ObjectId;
    quantity: Number;
}

const cartSchema = new Schema({
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    productId: {
        type: Schema.Types.ObjectId,
        ref: 'Product',
        required: true,
    },
    quantity: {
        type: Number,
        required: true,
        default: 0
    },
});

export default model<ICart>('Cart', cartSchema);
import { Schema, model, Document } from 'mongoose';
import bcrypt from 'bcryptjs';
import { IProduct } from './Product';

export interface IUser extends Document {
    username: string;
    email: string;
    password: string;
    cart: {
        items: {
            productId: Schema.Types.ObjectId,
            quantity: number
        },

    };
    encryptPassword(password: string): Promise<string>;
    validatePassword(password: string): Promise<string>;
    addToCart(product: IProduct): this;
}


const userSchema = new Schema({
    username: {
        type: String,
        required: true,
        min: 4,
        lowercase: true
    },
    email: {
        type: String,
        unique: true,
        required: true,
        lowercase: true
    },
    password: {
        type: String,
        required: true
    },
    cart: {
        items: [{
            productId: {
                type: Schema.Types.ObjectId,
                ref: 'Product',
                required: true
            },
            quantity: {
                type: Number,
                required: true
            }
        }]
    }
});


userSchema.methods.encryptPassword = async (password: string): Promise<string> => {
    const salt = await bcrypt.genSalt(10);
    return bcrypt.hash(password, salt);
}

userSchema.methods.validatePassword = async function (password: string): Promise<boolean> {
    return await bcrypt.compare(password, this.password);
}

userSchema.methods.addToCart = function (product: IProduct) {
    const cartProductIndex = this.cart.items.findIndex((cp: any) => {
        return cp.productId.toString() === product._id.toString();
    });
    let newQuantity = 1;
    const updatedCartItems = [...this.cart.items];

    if (cartProductIndex >= 0) {
        newQuantity = this.cart.items[cartProductIndex].quantity + 1;
        updatedCartItems[cartProductIndex].quantity = newQuantity;
    } else {
        updatedCartItems.push({
            productId: product._id,
            quantity: newQuantity
        });
    }
    const updatedCart = {
        items: updatedCartItems
    };
    this.cart = updatedCart;
    return this.save();
};

export default model<IUser>('User', userSchema);
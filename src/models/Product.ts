import { Schema, model, Document } from 'mongoose';
import bcrypt from 'bcryptjs';

export interface IProduct extends Document {
    Array: {
        title: string;
        description: string;
        price: string;
        make: Date;
    }
}

const productSchema = new Schema({
    title: {
        type: String,
        required: true,
    },
    description: {
        type: String,
    },
    make: {
        type: Date,
        default: Date.now
    },
    price: {
        type: Number,
        required: true
    }
});

export default model<IProduct>('Product', productSchema);
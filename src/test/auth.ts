
//During the test the env variable is set to test
process.env.NODE_ENV = 'test';
import { Error } from 'mongoose'
import Product from '../models/Product';

//Require the dev-dependencies
import chai from 'chai';
import chaiHttp from 'chai-http';
import server from '../app';
let should = chai.should();

chai.use(chaiHttp);
//Our parent block
describe('User', () => {

    //Test to register user with unique email
    describe('/POST signup', () => {
        it('it should Register user with unique emailId', (done) => {
            let user = {
                email: process.env.email || "testt1@gmail.com",
                username: process.env.username || "testt1111",
                password: process.env.password || "testt111"
            }
            chai.request(server)
                .post('/api/auth/signup')
                .send(user)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message').eq('Successfully Registered!');
                    done();
                });
        });

    });

    //Test to login user with valid credentials
    describe('/POST signin', () => {
        it('it should Login user if credentials is valid', (done) => {
            let user = {
                email: "test@gmail.com",
                password: "test"
            }
            chai.request(server)
                .post('/api/auth/signin')
                .send(user)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message').eq('successfully Logged In!');
                    res.body.should.have.property('success').eq(true)
                    done();
                });
        });

    });

});
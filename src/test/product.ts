//During the test the env variable is set to test
process.env.NODE_ENV = 'test';
import { Error } from 'mongoose'
import Product from '../models/Product';

//Require the dev-dependencies
import chai from 'chai';
import chaiHttp from 'chai-http';
import server from '../app';
let should = chai.should();


chai.use(chaiHttp);
//Our parent block
describe('Product', () => {
    // beforeEach((done) => { //Before each test we empty the database
    //     Product.remove({}, (err: Error) => {
    //         done();
    //     });
    // });

    /*
      * Test the /GET route
      */
    describe('/GET products', () => {
        it('it should GET all the products', (done) => {
            chai.request(server)
                .get('/products')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    done();
                });
        });
    });

    // Test To Post Route
    describe('/POST Product', () => {
        it('it should not POST a product without title and price field', (done) => {
            let product = {
                make: "2020-08-18",
                description: "Best camera under this price",
                price: 1954
            }
            chai.request(server)
                .post('/product')
                .send(product)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('errors');
                    res.body.errors.should.have.property('message');
                    done();
                });
        });

        it('it should POST a camera product ', (done) => {
            let product = {
                title: "Cam 1",
                description: "J.R.R. Tolkien",
                price: 1954,
                make: '2020-07-18'
            }
            chai.request(server)
                .post('/product')
                .send(product)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message').eql('Camera successfully added!');
                    res.body.camera.should.have.property('title');
                    res.body.camera.should.have.property('description');
                    res.body.camera.should.have.property('make');
                    res.body.camera.should.have.property('price');
                    done();
                });
        });

    });

    // Test to Cart Get Route
    describe('/GET cart', () => {
        it('it should GET all the cart products', (done) => {
            let user = {
                userId: process.env.userId || "5f1193f0ae46a7270cc562fc"
            }
            chai.request(server)
                .get('/cart')
                .send(user)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.data.should.be.a('array');
                    done();
                });
        });
    });

    //Test to Cart post Route
    describe('/POST Cart', () => {
        it('it should not Add a product to cart without Login or valid token', (done) => {
            let product = {
                userId: process.env.userId || "5f1193f0ae46a7270cc562fc",
                productId: process.env.productId || "5f12c74499d2f827c865dc4e",
            }
            chai.request(server)
                .post('/cart')
                .send(product)
                .end((err, res) => {
                    res.should.have.status(401);
                    res.body.should.be.a('object');
                    res.body.should.have.property("message").eql('Sorry!, Access denied!');
                    done();
                });
        });

    });


    describe('/POST Cart', () => {
        it('it should Add a product to cart with Login or valid token', (done) => {
            let product = {
                userId: process.env.userId || "5f1193f0ae46a7270cc562fc",
                productId: process.env.productId || "5f12c74499d2f827c865dc4e",
            }
            chai.request(server)
                .post('/cart')
                .send(product)
                .set({ authToken: process.env.authToken || 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZjExOTNmMGFlNDZhNzI3MGNjNTYyZmMiLCJpYXQiOjE1OTUwNjk1NzIsImV4cCI6MTU5NTE1NTk3Mn0.Va5Z7ajqhy7VOAMHjcY-ItyePk2XeOL2iES9hiLUs18' })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    done();
                });
        });

    });

});
import { Request, Response } from 'express';
import User, { IUser } from '../models/User';
import jwt from 'jsonwebtoken';

interface RequestWithUser extends Request {
    userId?: string;
}

export const signUp = async (req: Request, res: Response) => {
    //Find User from Databse
    const userEmailExist = await User.findOne({ email: req.body.email });

    //Check if value is null and if user exist
    if (req.body.email === null || req.body.username === null || req.body.password === null) {
        return res.status(204).json({ 'message': 'All the field required!' });
    }
    else if (userEmailExist) {
        return res.status(409).json({ 'message': 'This email is already taken' });
    }

    //Registering new user
    const user: IUser = new User({
        username: req.body.username,
        email: req.body.email,
        password: req.body.password,
    });
    user.password = await user.encryptPassword(user.password);
    const savedUser = await user.save();

    //Token 
    const token: string = jwt.sign({ id: savedUser._id }, process.env.SECRET_TOKEN || 'heyletsstart')
    res.header('authToken', token).status(200).json({ 'message': 'Successfully Registered!', savedUser });

}

export const signIn = async (req: Request, res: Response) => {
    //Check is input is null
    if (req.body.email === null || req.body.password === null) {
        return res.json({ 'success': false, 'message': 'All the field required!' });
    }
    const user = await User.findOne({ email: req.body.email });

    //check if email exist
    if (!user) return res.status(404).json({ 'success': false, 'message': 'Email or Password is wrong!' });
    const checkPassword = await user.validatePassword(req.body.password);

    if (!checkPassword) return res.status(404).json({ 'success': false, 'message': 'Password is not correct!' });
    const token = jwt.sign({ _id: user._id }, process.env.SECRET_TOKEN || 'heyletsstart', {
        expiresIn: 60 * 60 * 24
    });

    res.header('authToken', token).json({ 'success': true, 'message': 'successfully Logged In!', user });
}

export const profile = async (req: RequestWithUser, res: Response) => {
    const user = await User.findById(req.userId, { password: 0 });
    if (!user) return res.status(404).json('No User Found!');
    res.json(user);
}
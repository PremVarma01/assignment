import { Request, Response } from 'express';
import Product, { IProduct } from '../models/Product';
import User from '../models/User';
import Cart, { ICart } from '../models/Cart';
import mongoose, { Schema } from 'mongoose';

export const addProduct = async (req: Request, res: Response) => {
    if (!req.body.title || !req.body.price) {
        return res.json({ "errors": { "message": "title and price cannot be empty" } });
    }
    //Add Product
    const product: IProduct = new Product({
        title: req.body.title,
        description: req.body.description,
        price: req.body.price,
        make: new Date(req.body.make)
    });
    const camera = await product.save();
    res.status(200).json({ 'message': 'Camera successfully added!', camera });
}

export const getProducts = async (req: Request, res: Response) => {
    const product = await Product.find();
    if (!product) return res.status(404).json('No Product!');
    res.status(200).json(product);
}

export const addToCartUserModal = async (req: Request, res: Response) => {
    const prodId = req.body.productId;
    const userId = req.body.userId;
    const product = await Product.findById(prodId);
    const user = await User.findById(userId);
    if (product) {
        const resi = await user?.addToCart(product);
        console.log(resi);
        res.json(resi);
    } else {
        res.json({
            success: false,
            message: 'Smart, But you can\'t add product which is not valid!'
        });
    }
}

export const getCart = async (req: Request, res: Response) => {
    const userid = req.body.userId;
    if (!mongoose.Types.ObjectId.isValid(userid)) {
        return res.status(404).json({ "message": "please provide valid userId" })
    }
    const cart = await Cart.find({ userId: userid });
    if (cart) {
        res.json({
            success: true,
            data: cart
        });
    } else {
        res.json({
            success: true,
            message: 'Cart is empty!'
        });
    }
}

export const addToCart = async (req: Request, res: Response) => {
    const prodId = req.body.productId;
    const product = await Product.findById(prodId);
    if (product) {
        const cart = await Cart.findOne({ userId: req.body.userId, productId: prodId });
        let newQuantity: number = 1;
        let savedCart;
        if (cart?.productId.toString() === product?._id.toString()) {
            newQuantity = Number(cart?.quantity) + 1;
            savedCart = await Cart.findOneAndUpdate({ userId: req.body.userId, productId: req.body.productId }, {
                $set: {
                    quantity: newQuantity
                }
            }, { new: true });
        } else {
            const cartModel: ICart = new Cart({
                userId: req.body.userId,
                productId: req.body.productId,
                quantity: newQuantity
            });
            savedCart = await cartModel.save();
        }
        res.status(200).json({
            success: true,
            data: savedCart
        });
    } else {
        res.json({
            errors: {
                productId: "Product is not valid!"
            },
        });
    }
}


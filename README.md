# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact


# How to Use

* Firstly do "npm install" to install depenedency and devDependency.
* after installing all packages you can start server with "node run dev".

# How To Use Api.

Firstly Create your .env file in root directory and insert all these valid values:

SECRET_TOKEN=typescriptnodejs
MONGODB_URI=VALID_MONGODB_ATLAS_URI

You can use postman or other tools to run api

<----------------------------1 - Auth --------------------------------->

* Json Web Token have been used to secure route as well as to verify user using valid token.

* Register => Type "http://localhost:4000/api/auth/signup" with 

body:{
    email:'test@gmail.com',
    username:'test',
    password:'test'
}
method:"POST" 

method in POSTMAN to register user in MongoDb Database.

* Login => Type "http://localhost:4000/api/auth/signin" with 

body:{
    email:test@gmail.com,
    password:test
} 
method:"POST" 

method in POSTMAN to sign in user, header contains authToken after sucessfull login that you can use to access private routes.

<---------------------------2 Product--------------------------->


* AddProduct => Type "http://localhost:4000/product" with 

body:{
    title:'Canon D850',
    description:This is 60D camera to provide you more focus',
    price:12.12',
    make:2020-07-18 //YYYY-MM-DD
}
method:"POST" 

method in POSTMAN to add products in MongoDb Database.

* getProducts => Type "http://localhost:4000/products" with 
 
method:"GET" 

 in POSTMAN to get List of all available products from Database.


 <--------------------------3 Cart-------------------------->

 Cart can be add in two ways either they can be a serprate collection or can be embedded inside user modal. Both the ways has covered in this api.


* getCartByUserId : Simply Type "http://localhost:4000/cart" with 

body:{
    userId:'5f12ecb12646e5238c94ebb9'
}
method:"GET" 

 in POSTMAN to get List of available cart of user from Database.

 * addCart : Simply Type "http://localhost:4000/cart" with 

Note : Add cart required Valid user therefor you need to login and get "authToken" from header

header:{
    authToken:'YOUR_GENERATED_TOKEN'
}
body:{
    userId:'5f12ecb12646e5238c94ebb9',
    productId:'5f12ecb12646e5238c94ebb9'
}
method:"POST" 

 in POSTMAN to add Cart value of logged in user to database.



  * addCartToUserModal : Simply Type "http://localhost:4000/user/cart" with 

Note : Add cart required Valid user therefor you need to login and get "authToken" from header

header:{
    authToken:'YOUR_GENERATED_TOKEN'
}
body:{
    userId:'5f12ecb12646e5238c94ebb9',
    productId:'5f12ecb12646e5238c94ebb9'
}
method:"POST" 

 in POSTMAN to add Cart value of logged in user to database.


 # HOW to Test API

For api testing "MOCHA" And "CHAI" has been used in this project.
append following valid values to .env file in order to test api
you can also add these value inside test/product.ts

productId=VALID_PRODUCTID
userId=VALID_USERID
authToken=VALIDAUTHTOKEN
email=VALIDEMAIL
username=VALIDUSERNAME
password=VALIDPASSWORD

Now in terminal run "npm test"

It will show you all the test cases which has been passsed and failed

For the following Apis test cases has perfomed in this project.

signUp - To Check if unique email and valid values used to sign up.
signIn - To check if valid credentials user to sign in.

getProduct - To Check if products list is array and status is 200
addProduct without title - To Check if user trying to add product without title.
addProduct with all the detail - To check if product added successfully

getCart - To check if cart getting successfully
addCart without token - To Check if cart cannot be added without token
addCart with all values - To Check if product added into cart if valid token is provided








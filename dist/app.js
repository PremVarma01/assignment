"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const morgan_1 = __importDefault(require("morgan"));
require("./database");
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config();
const app = express_1.default();
const auth_1 = __importDefault(require("./routes/auth"));
const product_1 = __importDefault(require("./routes/product"));
//settings
app.set('port', 4000);
app.use(morgan_1.default('dev'));
app.use(express_1.default.json());
//routes
app.use('/api/auth', auth_1.default);
app.use(product_1.default);
app.listen(app.get('port'));
console.log("server on port", app.get('port'));
exports.default = app;
//# sourceMappingURL=app.js.map
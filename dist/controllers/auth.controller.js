"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.profile = exports.signIn = exports.signUp = void 0;
const User_1 = __importDefault(require("../models/User"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
exports.signUp = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    //Find User from Databse
    const userEmailExist = yield User_1.default.findOne({ email: req.body.email });
    //Check if value is null and if user exist
    if (req.body.email === null || req.body.username === null || req.body.password === null) {
        return res.status(204).json({ 'message': 'All the field required!' });
    }
    else if (userEmailExist) {
        return res.status(409).json({ 'message': 'This email is already taken' });
    }
    //Registering new user
    const user = new User_1.default({
        username: req.body.username,
        email: req.body.email,
        password: req.body.password,
    });
    user.password = yield user.encryptPassword(user.password);
    const savedUser = yield user.save();
    //Token 
    const token = jsonwebtoken_1.default.sign({ id: savedUser._id }, process.env.SECRET_TOKEN || 'heyletsstart');
    res.header('authToken', token).status(200).json({ 'message': 'Successfully Registered!', savedUser });
});
exports.signIn = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    //Check is input is null
    if (req.body.email === null || req.body.password === null) {
        return res.json({ 'success': false, 'message': 'All the field required!' });
    }
    const user = yield User_1.default.findOne({ email: req.body.email });
    //check if email exist
    if (!user)
        return res.status(404).json({ 'success': false, 'message': 'Email or Password is wrong!' });
    const checkPassword = yield user.validatePassword(req.body.password);
    if (!checkPassword)
        return res.status(404).json({ 'success': false, 'message': 'Password is not correct!' });
    const token = jsonwebtoken_1.default.sign({ _id: user._id }, process.env.SECRET_TOKEN || 'heyletsstart', {
        expiresIn: 60 * 60 * 24
    });
    res.header('authToken', token).json({ 'success': true, 'message': 'successfully Logged In!', user });
});
exports.profile = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const user = yield User_1.default.findById(req.userId, { password: 0 });
    if (!user)
        return res.status(404).json('No User Found!');
    res.json(user);
});
//# sourceMappingURL=auth.controller.js.map
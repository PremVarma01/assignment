"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.addToCart = exports.getCart = exports.addToCartUserModal = exports.getProducts = exports.addProduct = void 0;
const Product_1 = __importDefault(require("../models/Product"));
const User_1 = __importDefault(require("../models/User"));
const Cart_1 = __importDefault(require("../models/Cart"));
const mongoose_1 = __importDefault(require("mongoose"));
exports.addProduct = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    if (!req.body.title || !req.body.price) {
        return res.json({ "errors": { "message": "title and price cannot be empty" } });
    }
    //Add Product
    const product = new Product_1.default({
        title: req.body.title,
        description: req.body.description,
        price: req.body.price,
        make: new Date(req.body.make)
    });
    const camera = yield product.save();
    res.status(200).json({ 'message': 'Camera successfully added!', camera });
});
exports.getProducts = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const product = yield Product_1.default.find();
    if (!product)
        return res.status(404).json('No Product!');
    res.status(200).json(product);
});
exports.addToCartUserModal = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const prodId = req.body.productId;
    const userId = req.body.userId;
    const product = yield Product_1.default.findById(prodId);
    const user = yield User_1.default.findById(userId);
    if (product) {
        const resi = yield (user === null || user === void 0 ? void 0 : user.addToCart(product));
        console.log(resi);
        res.json(resi);
    }
    else {
        res.json({
            success: false,
            message: 'Smart, But you can\'t add product which is not valid!'
        });
    }
});
exports.getCart = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const userid = req.body.userId;
    if (!mongoose_1.default.Types.ObjectId.isValid(userid)) {
        return res.status(404).json({ "message": "please provide valid userId" });
    }
    const cart = yield Cart_1.default.find({ userId: userid });
    if (cart) {
        res.json({
            success: true,
            data: cart
        });
    }
    else {
        res.json({
            success: true,
            message: 'Cart is empty!'
        });
    }
});
exports.addToCart = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const prodId = req.body.productId;
    const product = yield Product_1.default.findById(prodId);
    if (product) {
        const cart = yield Cart_1.default.findOne({ userId: req.body.userId, productId: prodId });
        let newQuantity = 1;
        let savedCart;
        if ((cart === null || cart === void 0 ? void 0 : cart.productId.toString()) === (product === null || product === void 0 ? void 0 : product._id.toString())) {
            newQuantity = Number(cart === null || cart === void 0 ? void 0 : cart.quantity) + 1;
            savedCart = yield Cart_1.default.findOneAndUpdate({ userId: req.body.userId, productId: req.body.productId }, {
                $set: {
                    quantity: newQuantity
                }
            }, { new: true });
        }
        else {
            const cartModel = new Cart_1.default({
                userId: req.body.userId,
                productId: req.body.productId,
                quantity: newQuantity
            });
            savedCart = yield cartModel.save();
        }
        res.status(200).json({
            success: true,
            data: savedCart
        });
    }
    else {
        res.json({
            errors: {
                productId: "Product is not valid!"
            },
        });
    }
});
//# sourceMappingURL=product.controller.js.map
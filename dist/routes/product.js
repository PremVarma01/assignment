"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const validateToken_1 = require("../libs/validateToken");
const product_controller_1 = require("../controllers/product.controller");
const router = express_1.Router();
router.post('/product', product_controller_1.addProduct);
router.get('/products', product_controller_1.getProducts);
router.post('/user/cart', validateToken_1.TokenValidation, product_controller_1.addToCartUserModal);
router.post('/cart', validateToken_1.TokenValidation, product_controller_1.addToCart);
router.get('/cart', product_controller_1.getCart);
exports.default = router;
//# sourceMappingURL=product.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const productSchema = new mongoose_1.Schema({
    title: {
        type: String,
        required: true,
    },
    description: {
        type: String,
    },
    make: {
        type: Date,
        default: Date.now
    },
    price: {
        type: Number,
        required: true
    }
});
exports.default = mongoose_1.model('Product', productSchema);
//# sourceMappingURL=Product.js.map
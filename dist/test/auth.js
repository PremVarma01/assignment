"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
//During the test the env variable is set to test
process.env.NODE_ENV = 'test';
//Require the dev-dependencies
const chai_1 = __importDefault(require("chai"));
const chai_http_1 = __importDefault(require("chai-http"));
const app_1 = __importDefault(require("../app"));
let should = chai_1.default.should();
chai_1.default.use(chai_http_1.default);
//Our parent block
describe('User', () => {
    //Test to register user with unique email
    describe('/POST signup', () => {
        it('it should Register user with unique emailId', (done) => {
            let user = {
                email: process.env.email || "testt1@gmail.com",
                username: process.env.username || "testt1111",
                password: process.env.password || "testt111"
            };
            chai_1.default.request(app_1.default)
                .post('/api/auth/signup')
                .send(user)
                .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('message').eq('Successfully Registered!');
                done();
            });
        });
    });
    //Test to login user with valid credentials
    describe('/POST signin', () => {
        it('it should Login user if credentials is valid', (done) => {
            let user = {
                email: "test@gmail.com",
                password: "test"
            };
            chai_1.default.request(app_1.default)
                .post('/api/auth/signin')
                .send(user)
                .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('message').eq('successfully Logged In!');
                res.body.should.have.property('success').eq(true);
                done();
            });
        });
    });
});
//# sourceMappingURL=auth.js.map
"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TokenValidation = void 0;
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
exports.TokenValidation = (req, res, next) => {
    const token = req.header('authToken');
    if (!token)
        return res.status(401).json({ 'message': 'Sorry!, Access denied!' });
    try {
        const payload = jsonwebtoken_1.default.verify(token, process.env.SECRET_TOKEN || 'heyletsstart');
        req.userId = payload._id;
        next();
    }
    catch (err) {
        res.json({ 'message': 'This token is not valid or expired!' });
    }
};
//# sourceMappingURL=validateToken.js.map